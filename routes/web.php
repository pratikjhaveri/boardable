<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/home', function () {
    return view('home');
});

Route::middleware(['auth'])->group(function () {
	Route::get('/meetings', 'App\Http\Controllers\MeetingsController@index');
	Route::get('/meetings/{id}', 'App\Http\Controllers\MeetingsController@show');
	Route::post('/newMeeting','App\Http\Controllers\MeetingsController@create')->name('newMeeting');
	Route::get('/getMeeting', 'App\Http\Controllers\MeetingsController@getMeeting')->name('getMeeting');
	Route::get('/getMeetingTable','App\Http\Controllers\MeetingsController@meetingTable')->name('getMeetingTable');

});

Route::get('/meetings/{id}/public', 'App\Http\Controllers\MeetingsController@showPublic');

Route::post('/rsvpUser','App\Http\Controllers\RsvpController@rsvp')->name('rsvpUser');
Route::get('/getRsvpTable','App\Http\Controllers\RsvpController@rsvpTable')->name('getRsvpTable');

Auth::routes();
