<?php

namespace Database\Factories;

use App\Models\Rsvp;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;
use App\Models\Meeting;
class RsvpFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Rsvp::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'meeting_id' => Meeting::all()->random()->id,
            'user_id' => User::all()->random()->id,
            'rsvp' => $this->faker->numberBetween(0,2),
        ];
    }
}
