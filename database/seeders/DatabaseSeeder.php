<?php

namespace Database\Seeders;

use App\Models\Meeting;
use App\Models\User;
use App\Models\Rsvp;
use Database\Factories\MeetingFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        User::factory(20)->create();
        Meeting::factory(10)->create();
        Rsvp::factory(20)->create();
    }
}
