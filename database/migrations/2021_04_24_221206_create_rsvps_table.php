<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRsvpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rsvps', function (Blueprint $table) {
            $table->id();
            $table->integer('meeting_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('rsvp')->comment('0=no,1=yes,2=maybe');
            $table->timestamps();
            $table->foreign('meeting_id')
                    ->references('id')
                    ->on('meetings')
                    ->onCascade('delete');
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onCascade('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rsvps');
    }
}
