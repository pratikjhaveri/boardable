@extends('layouts.app')

@section('content')
	<div class="container">
	    <div class="row justify-content-center">
	        <div class="col-md-8">
	            <div class="card">
	                <div class="card-header">
	                	{{$meeting->title}}
	                	<span class="float-right"><a class="btn btn-primary" href="/meetings">&lt; Back</a></span>
	                </div>

	                <div class="card-body">
	                	<div class="alert" role="alert" style="display: none;"></div>
                    	<div class="row"> {{$meeting->description}} </div>
                    	<div class="row"> <b>Location: &nbsp;</b> {{$meeting->location}}</div>
                    	<div class="row"><b>Date and Time: &nbsp;</b>{{$meeting->start->format('Y M d @ H:i')}} - {{$meeting->end->format('Y M d @ H:i')}}</div>
                    	@if($public == '1')
	                    	@if($meeting->isPublic)
	                    		<div class="row"><b>Agenda: &nbsp;</b>{{$meeting->agenda}}</div>
	                    	@endif
                    	@else
                    		<div class="row"><b>Agenda: &nbsp;</b>{{$meeting->agenda}}</div>
                    	@endif
	                </div>
	            </div>

    			<!-- here is where a list of emails with their RSVP status for this meeting will go -->
	            <div class="card">
	            	<div class="card-header">
	            		RSVPs
	            		@if($public == '1')<span class="float-right"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#rsvpModal" data-meetingid="{{$meeting->id}}">RSVP Now</button></span>@endif
	            	</div>
	            	<div class="card-body">
            			<table class="table" id="rsvpTable">
            				<thead>
            					<th>Name</th>
            					<th>Email</th>
            					<th>Rsvp Status</th>
            				</thead>
            				<tbody>
            					@foreach($meeting->Rsvps as $rsvp)
	            					<tr>
		            					<td>{{$rsvp->users->name}}</td>
		            					<td>{{$rsvp->users->email}}</td>
		            					<td>{{getRsvpStatus($rsvp->rsvp)}}</td>
		            				</tr>
            					@endforeach
            				</tbody>
            			</table>
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>

{{-- MODAL STARTS HERE --}}

<div class="modal fade preview-modal" data-backdrop="" id="rsvpModal" tabindex="-1" role="dialog" aria-labelledby="rsvpModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="rsvpModalLabel">Your Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="rsvpForm" method="POST" action="{{ route('rsvpUser') }}">
      	<div class="modal-body">
        	@csrf
    		<input type="hidden" name="meetingId" id="meetingId">
          	<div class="form-group">
            	<label for="name" class="col-form-label">Name:</label>
            	<input type="text" class="form-control" id="name" name="name">
         	</div>
          	<div class="form-group">
            	<label for="email" class="col-form-label">Email:</label>
            	<input type="email" class="form-control" id="email" name="email">
          	</div>
          	<div class="form-group">
            	<label for="rsvp" class="col-form-label">Attending:</label>
            	<select class="form-control" id="rsvp" name="rsvp">
            		<option value="1">Yes</option>
            		<option value="0">No</option>            	
            		<option value="2">Maybe</option>
            	</select>
          	</div>  
      	</div>
      	<div class="modal-footer">
	    	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        	<button type="submit" id="rsvpButtonSubmit" class="btn btn-primary">Submit</button>
      	</div>
      </form>
    </div>
  </div>
</div>

{{-- MODAL ENDS --}}
    
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			//get the meeting id in the modal on modal open
			$('#rsvpModal').on('show.bs.modal', function (event) {
			  var button = $(event.relatedTarget) // Button that triggered the modal
			  var meetingId = button.data('meetingid') // Extract info from data-* attributes
			  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			  var modal = $(this)
			  modal.find('.modal-body #meetingId').val(meetingId)
			});

			//clear form on modal close
			$('#rsvpModal').on('hide.bs.modal', function(event){
				$('#rsvpForm').trigger('reset');
			});

			$('#rsvpForm').on('submit', function(e) {
				e.preventDefault();
				var form = $(this);
				var url = form.attr('action');
				$.ajax({
					type: "POST",
					url: url,
					data: form.serialize(),
					success: function(data)
					{
						$('.modal').each(function(){
		                    $(this).modal('hide');
		                });
						// $("#rsvpModal .close").click();
						if (data.msg == '1') {
			               	//rsvp success
			               	$('.alert').css('display','block');
			               	$('.alert').addClass('alert-success');
			               	$('.alert').text(data.text);
			               	resetRsvpTable();

		               	}else{
		               		//error
		               		$('.alert').css('display','block');
			               	$('.alert').addClass('alert-danger');
			               	$('.alert').text(data.text);
		               	} 
					}
          		});	
			});
		});
		
		function resetRsvpTable()
		{
			$('#rsvpTable tbody > tr').remove();
			var requestData = {};
	      	var token = '{{Session::token()}}';
	      	requestData['_token'] = token;
	      	requestData['mid'] = '{{$meeting->id}}';
			$.ajax({
					type: "GET",
					url: '{{ route('getRsvpTable') }}',
					data: requestData,
					success: function(data)
					{
						$('#rsvpTable tbody').append(data.html);
					}
				});
		}
	</script>
@endsection