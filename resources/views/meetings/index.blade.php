@extends('layouts.app')

@section('content')
	<div class="container">
	    <div class="row justify-content-center">
	        <div class="col-md-8">
				<div class="card">
					<div class="card-header">
						Meetings
						<span class="float-right"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#meetingModal" data-mtype="create">Add New Meeting</button></span>
					</div>
					<div class="card-body">
						<div class="alert" role="alert" style="display: none;"></div>
						<table class="table" id="meetingTable">
							<thead>
								<th>Name</th>
								<th>Action</th>
							</thead>
							<tbody>
								@foreach($meetings as $meeting)
									<tr>
				    					<td><a href="/meetings/{{$meeting->id}}">{{$meeting->title}}</a></td>
				    					<td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#meetingModal" data-mtype="edit" data-meetingid="{{$meeting->id}}">Edit</button></td>
				    				</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	{{-- MODAL STARTS HERE --}}

	<div class="modal fade preview-modal" data-backdrop="" id="meetingModal" tabindex="-1" role="dialog" aria-labelledby="meetingModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="meetingModalLabel"></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <form id="meetingForm" method="POST" action="{{ route('newMeeting') }}">
	      	<div class="modal-body">
	        	@csrf
	    		<input type="hidden" name="meetingId" id="meetingId">
	    		<input type="hidden" name="mtype" id="mtype">
	          	<div class="form-group">
	            	<label for="title" class="col-form-label">Name:</label>
	            	<input type="text" class="form-control" id="title" name="title">
	         	</div>
	         	<div class="form-group">
	            	<label for="location" class="col-form-label">Location:</label>
	            	<input type="text" class="form-control" id="location" name="location">
	         	</div>
	         	<div class="form-group">
	            	<label for="startDate" class="col-form-label">Start Date:</label>
	            	<div class="input-group date" id="startDate" data-target-input="nearest">
	                    <input type="text" class="form-control datetimepicker-input" data-target="#startDate"/>
	                    <div class="input-group-append" data-target="#startDate" data-toggle="datetimepicker">
	                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
	                    </div>
	                </div>
	          	</div>
	          	<div class="form-group">
	            	<label for="endDate" class="col-form-label">End Date:</label>
	            	<div class="input-group date" id="endDate" data-target-input="nearest">
	                    <input type="text" class="form-control datetimepicker-input" data-target="#endDate"/>
	                    <div class="input-group-append" data-target="#endDate" data-toggle="datetimepicker">
	                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
	                    </div>
	                </div>
	          	</div>
	          	<div class="form-group">
	            	<label for="description" class="col-form-label">Description:</label>
	            	<textarea class="form-control" id="description" name="description" rows="3"></textarea>
	          	</div>
	          	<div class="form-group">
	            	<label for="agenda" class="col-form-label">Agenda:</label>
	            	<textarea class="form-control" id="agenda" name="agenda" rows="3"></textarea>
	          	</div>
	          	<div class="form-group form-check">
				  	<input class="form-check-input" type="radio" name="isPublic" id="yes" value="1" checked>
				  	<label class="form-check-label" for="yes">
					    Visible Publicly
				  	</label>
				</div>
				<div class="form-group form-check">
				  	<input class="form-check-input" type="radio" name="isPublic" id="no" value="0">
					<label class="form-check-label" for="no">
					    Hidden
					</label>
				</div>
	      	</div>
	      	<div class="modal-footer">
		    	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        	<button type="submit" id="rsvpButtonSubmit" class="btn btn-primary">Submit</button>
	      	</div>
	      </form>
	    </div>
	  </div>
	</div>

{{-- MODAL ENDS --}}
@endsection

@section('scripts')

	<script type="text/javascript">
		$(document).ready(function(){
			$('#startDate').datetimepicker();
			$('#endDate').datetimepicker();

			$('#meetingModal').on('show.bs.modal', function (event) {
			  var button = $(event.relatedTarget); // Button that triggered the modal
			  var mtype = button.data('mtype'); // Extract info from data-* attributes
			  $('.modal-body #mtype').val(mtype);
			  if(mtype == 'edit')
			  {
			  	var meetingId = button.data('meetingid');
			  	// Initiate an AJAX request here (and then do the updating in a callback).
			  	var requestData = {};
		      	var token = '{{Session::token()}}';
		      	requestData['_token'] = token;
		      	requestData['mid'] = meetingId;
		      	$.ajax({
					type: "GET",
					url: '{{route('getMeeting')}}',
					data: requestData,
					success: function(data)
					{
						// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
					  	$('.modal-body #meetingId').val(data.id);
					  	$('.modal-body #title').val(data.title);
					  	$('.modal-body #description').val(data.description);
					  	$('.modal-body #location').val(data.location);
					  	$('.modal-body #agenda').val(data.agenda);
					  	$('.modal-body #startDate').datetimepicker('date', moment(data.start, 'YYYY-MM-DDTHH:mm:ss'));
					  	$('.modal-body #endDate').datetimepicker('date', moment(data.end, 'YYYY-MM-DDTHH:mm:ss'));
					  	if(data.isPublic == '1')
					  	{
					  		//check the check box
					  		$('.modal-body #yes').prop('checked',true);
					  	}else{
					  		$('.modal-body #no').prop('checked',true);
					  	}
					}
				});
			  }
			});

			//clear form on modal close
			$('#meetingModal').on('hide.bs.modal', function(event){
				$('#meetingForm').trigger('reset');
			});

			$('#meetingForm').on('submit', function(e) {
				e.preventDefault();
				var form = $(this);
				var url = form.attr('action');
				var formData = form.serializeArray();
				formData.push({ name: "start", value: $("#startDate").datetimepicker('date').format('YYYY-MM-DDTHH:mm:ss') });
				formData.push({ name: "end", value: $("#endDate").datetimepicker('date').format('YYYY-MM-DDTHH:mm:ss') });
				$.ajax({
					type: "POST",
					url: url,
					data: formData,
					success: function(data)
					{
						console.log(data.text);
						$('.modal').each(function(){
		                    $(this).modal('hide');
		                });
						// $("#rsvpModal .close").click();
						if (data.msg == '1') {
			               	//rsvp success
			               	$('.alert').css('display','block');
			               	$('.alert').addClass('alert-success');
			               	$('.alert').text(data.text);
			               	resetMeetingTable();

		               	}else{
		               		//error
		               		$('.alert').css('display','block');
			               	$('.alert').addClass('alert-danger');
			               	$('.alert').text(data.text);
		               	} 
					}
          		});	
			});
		});
		
		function resetMeetingTable()
		{
			$('#meetingTable tbody > tr').remove();
			var requestData = {};
	      	var token = '{{Session::token()}}';
	      	requestData['_token'] = token;
			$.ajax({
					type: "GET",
					url: '{{ route('getMeetingTable') }}',
					data: requestData,
					success: function(data)
					{
						$('#meetingTable tbody').append(data.html);
					}
				});
		}
	</script>
@endsection