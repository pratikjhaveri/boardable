# Boardable Coding Exercise

This repo is designed as a starting point to implement a "public meeting page" feature. A full description of the task and expected outcome is available in [ExerciseOverview.md](ExerciseOverview.md).

In order to run the application, you will need to:

1. Have an installed version of Laravel
2. Create a database (`touch database/database.sqlite`)
3. Run `php artisan migrate:install`
4. Run `php artisan migrate`
5. Run `php artisan db:seed`
6. Run your server (`php artisan serve` if not using Homestead or another virtualization technique)
7. Visit the application ( http://127.0.0.1:3000/meetings ) - you may need to adjust the port pending on how you serve the code
8. Log in with username: `test@boardable.com` and password: `password`

This was an Interesting Build. 

I have added the following feature/s apart from the requirement
1. added jobs queue 
2. queue jobs to send welcome email for new users
3. queue jobs to send rsvp confirmation email to users 
4. Edit Meetings 
5. Register a New User. 
6. Created factory for seeding db 


The Process: 
According to the docs 
1. The public page can be accessed via http://localhost:8000/meetings/{id}/public
	- on this page you can see the meeting details and a list of people who have rsvp'd. 
	- From this page you rsvp the visiting user for the meeting. 
	- In the RSVP form the user just enters the name and email and rsvps the user for that meeting 
		to Dos 	- the password is set to a default (in the future i would create a link which will be sent to the user for directly changing the password with password reset) 
				- Check for name and email validations more stringent. 
2. On the Meetings Page accessed via http://localhost:8000/meetings
	- first the meetings page is auth so without login you would be redirected to the login page. Once you login you will be redirected to the meeting page. 
	- On this page 
		- you can create a new meeting 
		- you can edit an existing meeting 
		- you can change the flag on the agenda (whether to show publicly or not) Which is also reflected on the public page
 

The emails are in working condition you can check the email layout with mailtrap.io 
just create a new account and update the .env file with the credentials 
and from the public page you can rsvp a new user as well as an existing user and see the emails sent out. 
You will have to change the .env file with QUEUE_CONNECTION=sync instead of database or you can run php artisan queue:work

I have created the queue so that the rsvp confirmation email goes to the rsvp queue and the welcome email goes to the email queue so that 
in the future you can assign priority for the queues to be runned. 

In addition to the above if time permitted i would have added a scheduled task which would have sent an email to all the rsvp users 1 day prior to the meeting date.
I would have also added password check for strong password and validations across the form in the whole application. 



