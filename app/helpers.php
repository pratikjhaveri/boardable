<?php 
use App\Models\User;
use App\Models\Rsvp;
use App\Models\Meeting;

function getRsvpStatus($status)
{
	switch($status)
	{
		case "0":
			return "No";
			break;
		case "1":
			return "Yes";
			break;
		case "2":
			return "Maybe";
			break;
		default:
			return "Not yet rsvp'd";
	}
}
