<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Meeting;
use App\Models\Rsvp;
use App\Models\User;

class MeetingsController extends Controller
{
    public function index()
    {
        $meetings = Meeting::all();

        return view('meetings.index')->with(['meetings' => $meetings]);
    }

    public function show($meetingId)
    {
        $meeting = Meeting::find($meetingId);

        return view('meetings.show')->with(['meeting' => $meeting,'public'=>'0']);
    }

    public function showPublic($meetingId)
    {
        $meeting = Meeting::with('rsvps.users')->find($meetingId);
        return view('meetings.show')->with(['meeting' => $meeting,'public'=>'1']);
    }

    public function getMeeting(Request $request)
    {
        $meeting = Meeting::find($request->mid);
        return $meeting->toArray();
    }

    public function create(Request $request)
    {
        $msg = ['msg'=>'1','text' => $request->all()];
        if($request->mtype == 'edit')
        {
            $meeting = Meeting::find($request->meetingId);
            $meeting->title = $request->title;
            $meeting->description = $request->description;
            $meeting->agenda = $request->agenda;
            $meeting->isPublic = $request->isPublic;
            $meeting->start = $request->start;
            $meeting->end = $request->end;
            $meeting->location = $request->location;
            $meeting->save(); 
            $msg = ['msg'=>'1','text' => 'Meeting Updated Successfully'];   
        }else{
            $meeting = new Meeting;
            $meeting->title = $request->title;
            $meeting->description = $request->description;
            $meeting->agenda = $request->agenda;
            $meeting->isPublic = $request->isPublic;
            $meeting->start = $request->start;
            $meeting->end = $request->end;
            $meeting->location = $request->location;
            $meeting->save();
            $msg = ['msg'=>'1','text' => 'Meeting Created Successfully'];
        }
        
        return response()->json($msg);
    }

    public function meetingTable(Request $request)
    {
        $meetings = Meeting::all();
        $html = "";
        foreach ($meetings as $meeting) {
            $html .= "<tr>";
            $html .= "<td><a href='/meetings/".$meeting->id."'>".$meeting->title."</a></td>";
            $html .= "<td><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#meetingModal' data-mtype='edit' data-meetingid='".$meeting->id."'>Edit</button></td>";
            $html .= "</tr>";   
        }
        return response()->json(['html' => $html]);
    }
}
