<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rsvp;
use App\Models\User;
use App\Models\Meeting;
use App\Jobs\welcomeEmailJob;
use App\Jobs\rsvpConfirmationJob;
use Hash;

class RsvpController extends Controller
{
    public function rsvp(Request $request)
    {
    	$msg = [];

    	//first create/retrive user 
    	$user = User::firstOrCreate(
    		['email' => $request->email],
    		['name' => $request->name, 'password' => Hash::make('password')]
    	);

    	//Check if User was created 
    	// If user was created then dispatch a job which will send a welcome email.
    	if($user->wasRecentlyCreated)
    	{
    		welcomeEmailJob::dispatch($user);
    	}

    	//rsvp user for the meeting
    	$checkRsvp = Rsvp::where('user_id',$user->id)->where('meeting_id',$request->meetingId)->first();
    	if(!$checkRsvp)
    	{
    		//Create a new rsvp model
    		$rsvp = new Rsvp;
	    	$rsvp->meeting_id = $request->meetingId;
	    	$rsvp->user_id = $user->id;
	    	$rsvp->rsvp = $request->rsvp;
	    	$rsvp->save(); 

	    	//send user email for rsvp confirmation 
	    	//check if rsvp was inserted 
	    	//send an email to user with confirmation of rsvp
	    	if(!$rsvp)
	    	{
	    		$msg = ['msg' => '0', 'text' => "Something Went Wrong! Please Contact Support for Further Assistance."];	    		
	    	}else{
	    		$meeting = Meeting::find($request->meetingId);

	    		rsvpConfirmationJob::dispatch($user,$meeting);

	    		$msg = ['msg' => '1', 'text' => "User successfully rsvp'd for this meeting."];
	    	}
    		
    	}else{

    		 //error user is already rsvpd
    		$msg = ['msg' => '0', 'text' => "User is already rsvp'd for this meeting."];   	
    	}

    	return response()->json($msg);
    }

    public function rsvpTable(Request $request)
    {
    	$meeting = Meeting::with('rsvps.users')->find($request->mid);
    	$html = "";
    	foreach ($meeting->rsvps as $rsvp) {
    		$html .= "<tr>";
			$html .= "<td>".$rsvp->users->name."</td>";
			$html .= "<td>".$rsvp->users->email."</td>";
			$html .= "<td>".getRsvpStatus($rsvp->rsvp)."</td>";
			$html .= "</tr>";	
    	}
    	return response()->json(['html' => $html]);
    }
}
