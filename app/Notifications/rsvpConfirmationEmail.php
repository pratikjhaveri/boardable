<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\Meeting;

class rsvpConfirmationEmail extends Notification implements ShouldQueue
{
    use Queueable;

    protected $meeting;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Meeting $meeting)
    {
        $this->meeting = $meeting;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        //Would have added google calendar link 
        return (new MailMessage)
                    ->greeting('Hello!, '.$notifiable->name)
                    ->line('This is a confirmation of rsvp for '.$this->meeting->title)
                    ->action('Please click here for more details', url('/meetings/'.$this->meeting->id.'/public'))
                    ->line('Thank you for using Boardable');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
