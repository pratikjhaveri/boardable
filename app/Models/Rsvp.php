<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rsvp extends Model
{
    use HasFactory;

    /**

     * Get the Rsvps for Meetings

     */

    public function meetings()
    {
        return $this->belongsTo(Meeting::class);
    }

    /**

     * Get the Users for Rsvps

     */

    public function users()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

}
