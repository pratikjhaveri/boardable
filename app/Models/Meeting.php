<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    use HasFactory;

    protected $dates = ['start', 'end'];

    protected $fillable = [
        'title', 'description', 'agenda', 'isPublic', 'start', 'end',
    ];
    /**

     * Get the Rsvps for Meetings

     */

    public function rsvps()
    {
        return $this->hasMany(Rsvp::class);
    }
}
